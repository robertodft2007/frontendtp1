import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NgxPaginationModule} from 'ngx-pagination';
import { UsopuntosComponent } from './usopuntos/usopuntos.component';
import { ReservaComponent } from './reserva/reserva.component';

const routes: Routes = [
    {path: '', redirectTo: '/appComponent',pathMatch: 'full'},
    {path: 'appComponent', component: AppComponent},
    {path: 'bolsaPuntos', component: ReservaComponent},
    {path: 'usoPuntos', component: UsopuntosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes),NgxPaginationModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
