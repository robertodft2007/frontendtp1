import { Local } from './../model/local.model';
import { Sucursal } from './../model/sucursal.model';
import { Servicio } from './../model/servicio.model';
import { Categoria } from './../model/categoria.model';

import { Component, OnInit } from '@angular/core';
import { ReservaService } from './reserva.service';
import { Reserva } from '../model/reserva.model';
import { arrayReserva } from '../model/arrayreserva.model';
import { Persona } from '../model/persona.model';
import { Especialidad } from '../model/especialidad.model';
import { stringify } from '@angular/compiler/src/util';
import { isUndefined } from 'util';
declare var $: any;

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css'],
  providers:[ReservaService]
})
export class ReservaComponent implements OnInit {
  private bolsaPuntos:any;
  private clientes:any
  private clienteSelected:string;
  private estadoSelected:string;
  private vencimiento:string;
  constructor(private reservaService:ReservaService) { }

  
  ngOnInit() {
    this.loadClientes();
    this.loadBolsas();
  }
  buscar(){

    if(isUndefined(this.clienteSelected) || this.clienteSelected=="Todos"){
      console.log("Es undefined");
      this.clienteSelected="todos";      
    }  
    if(isUndefined(this.estadoSelected) || this.estadoSelected=="Todos"){
      console.log("Es undefined");
      this.estadoSelected="todos";      
    }  
    if(isUndefined(this.vencimiento) || this.vencimiento==""){
      console.log("Es undefined");
      this.vencimiento="todos"; 
    }
    this.reservaService.getBolsaEspecifica(this.clienteSelected,this.estadoSelected,this.vencimiento).subscribe(res=>{
      this.bolsaPuntos=res;
      console.log(res);
    });
  }
  cambioCliente(event:any){
    console.log(event.target.value);
    this.clienteSelected=event.target.value;
  }
  cambioEstado(event:any){
    console.log(event.target.value);
    this.estadoSelected=event.target.value;
  }
  private loadBolsas():void{
    this.reservaService.getBolsaPunto().subscribe(res=>{
      this.bolsaPuntos=res;
      console.log(res);
    });
  }
  private loadClientes():void{
    this.reservaService.getCliente().subscribe(res=>{
      this.clientes=res;
      console.log(res);
    });
  }
}