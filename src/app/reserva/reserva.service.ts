
import { PARAMETERS } from '@angular/core/src/util/decorators';
import { Servicio } from './../model/servicio.model';
import { Persona } from './../model/persona.model';
import { Sucursal } from './../model/sucursal.model';
import { Local } from './../model/local.model';
import { Categoria } from './../model/categoria.model';

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Reserva } from './../model/reserva.model';
import { arrayReserva } from '../model/arrayreserva.model';
import { Especialidad } from '../model/especialidad.model';
import { EspecialidadLista } from '../model/especialidadlista.model';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ReservaService {
  constructor(private http:HttpClient) { }

  public getBolsaPunto():Observable<any>{
    return this.http.get("http://localhost:3000/sfc/bolsapunto/todos/todos/todos");
  }
  public getBolsaEspecifica(cliente:string,estado:string,vencimiento:string):Observable<any>{
    var url:string="http://localhost:3000/sfc/bolsapunto/"+cliente+"/"+estado+"/"+vencimiento;
    console.log(url);
    return this.http.get(url);
  }
  
  public getCliente():Observable<any>{
    return this.http.get("http://localhost:3000/sfc/cliente");
  }
  
}
