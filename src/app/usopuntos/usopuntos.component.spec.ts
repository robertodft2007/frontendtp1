import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsopuntosComponent } from './usopuntos.component';

describe('UsopuntosComponent', () => {
  let component: UsopuntosComponent;
  let fixture: ComponentFixture<UsopuntosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsopuntosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsopuntosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
