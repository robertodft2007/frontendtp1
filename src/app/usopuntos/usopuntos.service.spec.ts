import { TestBed } from '@angular/core/testing';

import { UsopuntosService } from './usopuntos.service';

describe('UsopuntosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsopuntosService = TestBed.get(UsopuntosService);
    expect(service).toBeTruthy();
  });
});
