import { UsopuntosService } from './usopuntos.service';
import { Component, OnInit } from '@angular/core';
import { ThrowStmt } from '@angular/compiler';
import { isUndefined } from 'util';
declare var $: any;
@Component({
  selector: 'app-usopuntos',
  templateUrl: './usopuntos.component.html',
  styleUrls: ['./usopuntos.component.css']
})
export class UsopuntosComponent implements OnInit {
  private usopuntos:any;
  private clientes:any;
  private conceptos:any;
  private clienteSelected:string;
  private conceptoSelected:string;
  private clienteSelected2:string;
  private conceptoSelected2:string;
  private fechaDesde:string;
  private fechaHasta:string;
  private utilizarPuntosDialog:string;
  private display='none';
  constructor(private reservaService:UsopuntosService) { }

  
  ngOnInit() {
    this.loadConcepto();
    this.loadClientes();
    this.loadUsos();    
  }
  abrirModal(){
    this.utilizarPuntosDialog=""
    this.display='block';
  }
  cerrar(){
    this.display='none';
  }
  async cerrarModal(){
    this.display='none';
    await this.reservaService.postUtilizarPuntos(this.clienteSelected2,this.conceptoSelected2);
    //mientras se arregla lo de Promise
    this.buscar();
    this.buscar();
    this.buscar();
    this.buscar();

  }
  utilizarPuntos(){
    this.abrirModal();
  
  }
  buscar(){
    if(isUndefined(this.conceptoSelected) || this.conceptoSelected=="Todos"){
      console.log("Es undefined");
      this.conceptoSelected="todos";      
    }  
    if(isUndefined(this.clienteSelected) || this.clienteSelected=="Todos"){
      console.log("Es undefined");
      this.clienteSelected="todos";      
    }  
    if(isUndefined(this.fechaDesde) || this.fechaDesde=="2019-05-17"){
      console.log("Es undefined");
      this.fechaDesde="2019-05-17";
    }
    if(this.fechaDesde==""){
      this.fechaDesde="todos";
    }
    if(isUndefined(this.fechaHasta) || this.fechaHasta=="2019-05-17"){
      console.log("Es undefined");
      this.fechaHasta="2019-05-17";
    }
    if(this.fechaHasta==""){
      this.fechaHasta="todos";
    }

    console.log(this.conceptoSelected);
    console.log(this.clienteSelected);
    console.log(this.fechaDesde);
    console.log(this.fechaHasta);
    this.reservaService.getUsoPuntoEspecifico(this.conceptoSelected,this.clienteSelected,this.fechaDesde,this.fechaHasta).subscribe(res=>{
      this.usopuntos=res;
      console.log(res);
    });
  }
  cambioCliente(event:any){
    console.log(event.target.value);
    this.clienteSelected=event.target.value;
  }
  cambioConcepto(event:any){
    console.log(event.target.value);
    this.conceptoSelected=event.target.value;
  }
  cambioCliente2(event:any){
    console.log(event.target.value);
    this.clienteSelected2=event.target.value;
  }
  cambioConcepto2(event:any){
    console.log(event.target.value);
    this.conceptoSelected2=event.target.value;
  }
  private loadUsos():void{
    this.reservaService.getUsoPunto().subscribe(res=>{
      this.usopuntos=res;
      console.log(res);
    });
  }
  private loadClientes():void{
    this.reservaService.getCliente().subscribe(res=>{
      this.clientes=res;
      console.log(res);
    });
  }
  private loadConcepto():void{
    this.reservaService.getConcepto().subscribe(res=>{
      this.conceptos=res;
      console.log(res);
    });
  }

}