import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'


@Injectable({
  providedIn: 'root'
})
export class UsopuntosService {
  constructor(private http:HttpClient) { }

  public getUsoPunto():Observable<any>{
    return this.http.get("http://localhost:3000/sfc/usopunto/todos/2017-05-02/2019-05-02/1");
  }
  public getUsoPuntoEspecifico(concepto:string,cliente:string,fechaDesde:string,fechaHasta:string):Observable<any>{
    var url:string="http://localhost:3000/sfc/usopunto/"+concepto+"/"+fechaDesde+"/"+fechaHasta+"/"+cliente;
    console.log(url);
    return this.http.get(url);
  }
  public getCliente():Observable<any>{
    return this.http.get("http://localhost:3000/sfc/cliente");
  }
  public getConcepto():Observable<any>{
    return this.http.get("http://localhost:3000/sfc/punto");
  }

  public async postUtilizarPuntos(cliente:string,concepto:string){
    console.log(cliente+"llego");
    console.log(concepto+"llego");
    
    var url:string="http://localhost:3000/sfc/usopunto";
      const body = new HttpParams()
        .set('cliente', cliente)
        .set('concepto_punto', concepto);
    
      await this.http.post(url,
        body.toString(),
        {
          headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')
        }
      ).subscribe(
        (res) => {
            console.log(res);
        },
        err => console.log(err));
    }
}
