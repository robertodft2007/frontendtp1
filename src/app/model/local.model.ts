import { Categoria } from './categoria.model';
import { Persona } from './persona.model';

export class Local{
    public idLocal: number;
    public descripcion: string;
    public nombre: string;
    public categorias: Array<Categoria>;
    public persona: Array<Persona>;
}