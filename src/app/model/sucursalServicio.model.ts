import { BigDecimal } from 'bigdecimal';
import { Servicio } from './servicio.model';
import { PersonaSucursalServicio } from './personaSucursalServicio.model';
import { Reserva } from './reserva.model';
import { Sucursal } from './sucursal.model';
export class SucursalServicio{
    public idSucursalServicio: number;
    public capacidad: number;
    public duracion: number;
    public precio:BigDecimal;
    public personaSucursalServicios:Array<PersonaSucursalServicio>;
    public reservas:Array<Reserva>;
    public servicio:Servicio;
    public sucursal:Sucursal;
}