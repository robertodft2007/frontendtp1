import { Timestamp } from 'rxjs';
import { Time } from '@angular/common';
import { Persona } from './persona.model';
import { SucursalServicio } from './sucursalServicio.model';


export class Reserva{
    public idReserva: number;
    public fecha: Date;
    public fechaHoraCreacion: Date;
    public flagAsistio:string;
    public flagEstado:string;
    public horaFin:Time;
    public horaInicio:Time;
    public observacion:string;
    public persona1:Persona;
    public persona2:Persona;
    public sucursalServicio:SucursalServicio;
}