import { Especialidad } from './especialidad.model';
export class EspecialidadLista{
    public lista: Array<Especialidad>;
    public total: number;
}