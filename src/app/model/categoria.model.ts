import { Local } from './local.model';

export class Categoria{
    public idCategoria: number;
    public nombre: string;
}