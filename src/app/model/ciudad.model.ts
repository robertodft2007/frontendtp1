import { Sucursal } from './sucursal.model';

export class Ciudad{
    public idCiudad: number;
    public nombre:string;
    public sucursal:Array<Sucursal>;
}