import { Persona } from './persona.model';

export class Agenda{
    public idAgenda: number;
    public actividad: string;
    public fecha: Date;
    public idPersona: Persona;
}