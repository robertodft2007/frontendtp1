import { Time } from '@angular/common';
import { Ciudad } from './ciudad.model';
import { HorarioExcepcion } from './horarioExcepcion.model';
import { Local } from './local.model';
import { Mapa } from './mapa.model';
import { SucursalServicio } from './sucursalServicio.model';

export class Sucursal{
    public domingoHoraApertura: Time;
    public domingoHoraCierre:Time;
    public idLocal:Number;
    public juevesHoraApertura:Time;
    public juevesHoraCierre:Time;
    public lunesHoraApertura:Time;
    public lunesHoraCierre:Time;
    public martesHoraApertura:Time;
    public martesHoraCierre:Time;
    public miercolesHoraApertura:Time;
    public miercolesHoraCierre:Time;
    public nombre:string;
    public sabadoHoraApertura:Time;
    public sabadoHoraCierre:Time;
    public viernesHoraApertura:Time;
    public viernesHoraCierre:Time;
    public horarioExcepcions:Array<HorarioExcepcion>;
    public ciudad:Ciudad;
    public local:Local;
    public mapa:Mapa;
    public sucursalServicios:Array<SucursalServicio>;

}