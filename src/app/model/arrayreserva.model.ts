import { Persona } from './persona.model';
import { Local } from './local.model';
import { Reserva } from './reserva.model';
import { Sucursal } from './sucursal.model';
import { Especialidad } from './especialidad.model';
import { Servicio } from './servicio.model';

export class arrayReserva{
    public reserva:Reserva;
    public local:Local;
    public sucursal:Sucursal;
    public persona1:Persona;
    public especialidad:Especialidad;
    public servicio:Servicio;
    public persona2:Persona;

}