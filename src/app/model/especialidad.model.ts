import { Servicio } from './servicio.model';
export class Especialidad{
    public idEspecialidad: number;
    public nombre: string;
    public servicios:Array<Servicio>;
}