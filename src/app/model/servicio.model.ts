import { Especialidad } from './especialidad.model';
import { SucursalServicio } from './sucursalServicio.model';

export class Servicio{
    public idServicio: number;
    public duracionReferencia:number;
    public nombre: string;
    public especialidad:Especialidad;
    public sucursalServicios:SucursalServicio;
}