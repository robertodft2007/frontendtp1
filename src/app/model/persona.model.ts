import { Local } from './local.model';
import { PersonaSucursalServicio } from './personaSucursalServicio.model';
import { Reserva } from './reserva.model';

export class Persona{
    public idPersona: number;
    public apellido: string;
    public email: string;
    public fechaNacimiento: Date;
    public flagEmpleado: string;
    public nombre: string;
    public numeroDocumento: string;
    public usuario: string;
    public local: Local;
    public personaSucursalServicios:Array<PersonaSucursalServicio>;
    public reservas1:Array<Reserva>;
    public reservas2:Array<Reserva>;
}