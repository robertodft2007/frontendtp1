import { BigDecimal } from 'bigdecimal';
import { Persona } from './persona.model';
import { SucursalServicio } from './sucursalServicio.model';
export class PersonaSucursalServicio{
    public idPersonaSucursalServicio: number;
    public precio: BigDecimal;
    public persona:Persona;
    public sucursalServicio: SucursalServicio;

}