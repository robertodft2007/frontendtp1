import { Time } from '@angular/common';
import { Sucursal } from './sucursal.model';

export class HorarioExcepcion{
    public idHorarioExcepcion: number;
    public fecha: Date;
    public horaApertura: Time;
    public horaCierre:Time;
    public idSucursal:Sucursal;
}