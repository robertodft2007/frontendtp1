import { BigDecimal } from 'bigdecimal';
import { Sucursal } from './sucursal.model';
export class Mapa{
    public idMapa: number;
    public direccion: string;
    public latitud:BigDecimal;
    public longitud:BigDecimal;
    public nombre: string;
    public sucursals:Array<Sucursal>;
}